let start = Date.now()

let header = document.createElement('div');
header.className = 'header';

let aboutFuture = document.createElement('div');
aboutFuture.className = 'about-future';

let courses = document.createElement('div');
courses.className = 'courses';

let user = document.createElement('div');
user.className = 'user';

let socialLinks = document.createElement('div');
socialLinks.className = 'social-links';

header.append(user, socialLinks);

let imgBystrov = document.createElement('img');
imgBystrov.src = 'img/bystrov.png';
imgBystrov.alt = 'Фото Быстрова Б.В.'

let userData = document.createElement('div');
userData.className = 'user-data';

user.append(imgBystrov, userData);

let userDataFio = document.createElement('div');
userDataFio.className = 'user-data_fio';
userDataFio.innerHTML = 'Быстров Борис Викторович';

let userDataEmail = document.createElement('a');
userDataEmail.className = 'link user-data_email';
userDataEmail.href = 'mailto:bystrov@gmail.com'
userDataEmail.innerHTML = 'bystrov@gmail.com';

userData.append(userDataFio, userDataEmail);

let linkGit = document.createElement('a');
linkGit.className = 'link social-links_github';
let fabFaGithub = document.createElement('i');
fabFaGithub.className = 'fab fa-github';
linkGit.append(fabFaGithub);

let linkSkype = linkGit.cloneNode(true);
linkSkype.className = 'link social-links_skype'
linkSkype.childNodes[0].className = 'fab fa-skype';

let linkVK = linkGit.cloneNode(true);
linkVK.className = 'link social-links_vkontakte';
linkVK.childNodes[0].className = 'fab fa-vk';

socialLinks.append(linkGit, linkSkype, linkVK);

//////////////////////////ABOUT-FUTURE////////////////////////////////

let h1 = document.createElement('h1');
let h2 = document.createElement('h2');
h1.innerHTML = 'О будущем ученике';
h2.innerHTML = 'Ребятам из Loftschool будет полезно узнать немного обо мне';

let aboutMe = document.createElement('div');
aboutMe.className = 'about-me';

let ok = document.createElement('div');
ok.className = 'ok';

let imgHello = document.createElement('img');
imgHello.src = 'img/hello.png';
imgHello.alt = 'Передняя часть ладони'

let description = document.createElement('div');
description.className = 'description';

let descriptionH = document.createElement('p');
descriptionH.className = 'description_h';
descriptionH.innerHTML = 'Немного обо мне';

let descriptionText = document.createElement('div');

descriptionText.innerHTML = `Я живу в Санкт-Петербурге (Россия). Мне <br>
26 лет. Высшее образование - закончил <br>
НИУ ИТМО. Опыт в разработке 1 год. Уже <br>
окончил курсы в Академии <br>
Программирования, где не плохо <br>
научился верстать. <br>
Пока есть только один сайт, которым могу <br>`

let descriptionWordLink = document.createElement('div');
descriptionWordLink.className = 'description-word-link'
let word = document.createElement('div');
word.innerText = `похвастаться:`
let linkYandex =  document.createElement('a');
linkYandex.href = 'https://yandex.ru/';
linkYandex.innerHTML = 'www.yandex.ru';

descriptionWordLink.append(word, linkYandex)
description.append(descriptionH, descriptionText, descriptionWordLink);
ok.append(imgHello, description);

let ok2 = ok.cloneNode(true);
ok2.childNodes[0].src = 'img/experience.png';
ok2.childNodes[1].childNodes[0].innerHTML = 'Мой опыт в разработке';
ok2.childNodes[1].childNodes[1].innerHTML = `В этом блоке я расскажу о себе, что я уже<br>
умею. Здесь я укажу другие курсы, которые<br>
до этого я уже проходил. Могу<br>
перечислить технологии, которыми уже<br>
умею пользоваться. К примеру: html5, css3,<br>
sass, jade, git и пр.`;
ok2.childNodes[1].childNodes[2].remove();
aboutMe.append(ok, ok2);
aboutFuture.append(h1, h2, aboutMe);


/////////////////////////КУРСЫ////////////////////////////////

let h21 = h1.cloneNode(true);
let h22 = h2.cloneNode(true);
h21.innerHTML = 'Курсы на loftblog';
h22.innerHTML = 'которые мне помогут в выполнении этого задания';

let coursesForms = document.createElement('div');
coursesForms.className = 'courses_forms';

function getListContent() {
  let fragment = new DocumentFragment();

  for(let i=1; i<=100000; i++) {

    let courseJS = document.createElement('div');
    courseJS.className = 'course JS'
    
    let imgJS = document.createElement('img');
    imgJS.src = 'img/courses/js.png'
    
    let courseDescription =  document.createElement('div');
    courseDescription.className = 'course_description';
    
    let relative =  document.createElement('div');
    relative.className = 'relative';
    
    let absolute =  document.createElement('div');
    absolute.className = 'absolute';
    
    let h3 = document.createElement('h3');
    h3.innerHTML = 'Основы JavaScript'
    let h4 = document.createElement('h4');
    h4.innerHTML = `Видеокурс о одном из самых популярных языков программирования - JavaScript.
    Изучая этот курс вы познаете этот прекрасный язык от самых основ.`
    
    let clock = document.createElement('p');
    let farFaClock = document.createElement('i');
    farFaClock.className = 'far fa-clock';
    let time =  document.createElement('div');
    time.innerText = `${i} урока (4 часов 46 минут)`;
    clock.append(farFaClock, time);
    relative.append(absolute);
    
    let checkbox = document.createElement('div');
    checkbox.className = 'completed';
    let input = document.createElement('input');
    input.id = `chkbx${i}`;
    input.type = 'checkbox';
    input.className = 'fas';
    let label = document.createElement('label');
    label.setAttribute('for', `chkbx${i}`);
    label.innerHTML = 'Посмотрел';
    
    checkbox.append(input, label);
    courseDescription.append(relative, h3, h4, clock, checkbox);
    courseJS.append(imgJS, courseDescription);
    fragment.append(courseJS);
  }

  return fragment;
}

coursesForms.append(getListContent())


// let courseJS = document.createElement('div');
// courseJS.className = 'course JS'

// let imgJS = document.createElement('img');
// imgJS.src = 'img/courses/js.png'

// let courseDescription =  document.createElement('div');
// courseDescription.className = 'course_description';

// let relative =  document.createElement('div');
// relative.className = 'relative';

// let absolute =  document.createElement('div');
// absolute.className = 'absolute';

// let h3 = document.createElement('h3');
// h3.innerHTML = 'Основы JavaScript'
// let h4 = document.createElement('h4');
// h4.innerHTML = `Видеокурс о одном из самых популярных языков программирования - JavaScript.
// Изучая этот курс вы познаете этот прекрасный язык от самых основ.`

// let clock = document.createElement('p');
// let farFaClock = document.createElement('i');
// farFaClock.className = 'far fa-clock';
// clock.innerHTML =  '24 урока (6 часов 46 минут)';
// clock.append(farFaClock);
// relative.append(absolute);

// let checkbox = document.createElement('div');
// checkbox.className = 'completed';
// let input = document.createElement('input');
// input.id = 'chkbx1';
// input.type = 'checkbox';
// input.className = 'fas';
// let label = document.createElement('label');
// label.setAttribute('for', 'chkbx1');
// label.innerHTML = 'Посмотрел';

// checkbox.append(input, label);
// courseDescription.append(relative, h3, h4, clock, checkbox);
// courseJS.append(imgJS, courseDescription);

// let jQuery = courseJS.cloneNode(true);
// jQuery.childNodes[0].src = 'img/courses/jquery.png';
// jQuery.childNodes[1].childNodes[1].innerHTML = 'Основы JQUERY'
// jQuery.childNodes[1].childNodes[3].innerHTML =  `11 уроков (2 часа 6 минут)`;
// jQuery.childNodes[1].childNodes[3].append(farFaClock);
// jQuery.childNodes[1].childNodes[4].childNodes[0].id = 'chkbx2'
// jQuery.childNodes[1].childNodes[4].childNodes[1].setAttribute('for', 'chkbx2');

// let baseEl = courseJS.cloneNode(true);
// baseEl.childNodes[0].src = 'img/courses/base.png';
// baseEl.childNodes[1].childNodes[1].innerHTML = 'Базовые элементы страницы'
// baseEl.childNodes[1].childNodes[2].innerHTML = `Видеокурс по верстке и программированию основных и популярных элементов
// сайтов. Посмотрев этот курс вы научитесь создавать табы, аккордеон, 
// слайдшоу, слайдер и т.д.`
// baseEl.childNodes[1].childNodes[3].innerHTML =  ` 9 уроков (2 часа 7 минут)`;
// baseEl.childNodes[1].childNodes[3].append(farFaClock);
// baseEl.childNodes[1].childNodes[4].childNodes[0].id = 'chkbx3'
// baseEl.childNodes[1].childNodes[4].childNodes[1].setAttribute('for', 'chkbx3');

// let adaptive = courseJS.cloneNode(true);
// adaptive.childNodes[0].src = 'img/courses/adaptive.png';
// adaptive.childNodes[1].childNodes[1].innerHTML = 'Создание адаптивных сайтов'
// adaptive.childNodes[1].childNodes[2].innerHTML = `
// В этом курсе вы узнаете как создавать
// адаптивные сайты, что такое адаптивная верстка и дизайн.`
// adaptive.childNodes[1].childNodes[3].innerHTML =  `9 уроков (2 часа 7 минут)`;
// adaptive.childNodes[1].childNodes[3].append(farFaClock);
// adaptive.childNodes[1].childNodes[4].childNodes[0].id = 'chkbx4'
// adaptive.childNodes[1].childNodes[4].childNodes[1].setAttribute('for', 'chkbx4');



// let gallery = courseJS.cloneNode(true);
// gallery.childNodes[0].src = 'img/courses/button.png';
// gallery.childNodes[1].childNodes[1].innerHTML = 'Верстка адаптивной css галереи'
// gallery.childNodes[1].childNodes[2].innerHTML = `В этом видеокурсе вы изучите инструменты, техники и подходы для web-разработчика,
// которые помогут вам решать поставленные задачи более быстро и эффективно.`
// gallery.childNodes[1].childNodes[3].innerHTML =  ` 6 уроков (1 час 9 минут)`;
// gallery.childNodes[1].childNodes[3].append(farFaClock);
// gallery.childNodes[1].childNodes[4].childNodes[0].id = 'chkbx5'
// gallery.childNodes[1].childNodes[4].childNodes[1].setAttribute('for', 'chkbx5');

// let gallery2 = courseJS.cloneNode(true);
// gallery2.childNodes[0].src = 'img/courses/gallery.png';
// gallery2.childNodes[1].childNodes[1].innerHTML = 'Верстка адаптивной css галереи'
// gallery2.childNodes[1].childNodes[2].innerHTML = `В этом курсе мы разберем как сверстать адаптивную css галерею.
// Также познакомимся с Flexbox.`
// gallery2.childNodes[1].childNodes[3].innerHTML =  ` 4 урокa (50 минут)`;
// gallery2.childNodes[1].childNodes[3].append(farFaClock);
// gallery2.childNodes[1].childNodes[4].childNodes[0].id = 'chkbx6'
// gallery2.childNodes[1].childNodes[4].childNodes[1].setAttribute('for', 'chkbx6');

// coursesForms.append(courseJS);
courses.append(h21 , h22, coursesForms);


/////////////////////////КУРСЫ////////////////////////////////


let footer = document.createElement('footer');
let footerText = document.createElement('text');
footerText.innerHTML = `А это футер, с текстом внутри. Он должен быть прибит к низу окна браузера,<br>
не зависимо от количества контента на странице и размера окна.`
footer.append(footerText)

document.body.append(header, aboutFuture, courses, footer);
let end = Date.now()
console.log(end - start);
