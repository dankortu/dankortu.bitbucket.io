const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {
  entry: 
    './src/app/index.js',
    
  output: {
      filename: 'main.js',
      path: path.resolve(__dirname, '')
    },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          "babel-loader"
        ]
        
      },
      
      {
        test: /\.css$/,
        use: [
            "style-loader",
            "css-loader"
        ]
      },

      {
        test: /\.(sass|scss)$/,
        use: [
            "style-loader",
            "css-loader",
            "sass-loader"
        ]
      },

      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
            "file-loader"
        ]
      },
      
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/template/index.html",
    }),
  ],
};
 