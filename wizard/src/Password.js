import React from 'react';

const Password = (props) => {
  const { handler, valueInputPass, valueInputPass2 } = props;

  return (
    <div className="user-data">
      <h1>Введите данные</h1>

      <div className="password field">
        <p>Пароль:</p>
        <input
          name="password"
          type="password"
          placeholder="Минимум 8 символов"
          onChange={handler}
          value={valueInputPass}
          autoFocus
        />
      </div>
      <div className="password2 field">
        <p>Еще раз:</p>
        <input
          name="password2"
          type="password"
          onChange={handler}
          value={valueInputPass2}
        />
      </div>
    </div>
  );
};

export default Password;
