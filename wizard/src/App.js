import React, { Component } from 'react';
import UserData from './UserData';
import Contacts from './Contacts';
import Password from './Password';
import Card from './Card';
import WizardNavigation from './WizardNavigation';
import Buttons from './Buttons';
import {
  REGISTER_BUTTON_PAGE_NUM,
  USER_DATA_PAGE_NUM,
  CONTACTS_PAGE_NUM,
  CARD_PAGE_NUM,
  PASSWORD_PAGE_NUM,
  CONGRATULATIONS_PAGE_NUM,
  REG_NAME,
  REG_PHONE,
  REG_EMAIL,
  REG_CARD,
} from './Constants';

class Wrapper extends Component {
  state = {
    pageNumber: REGISTER_BUTTON_PAGE_NUM,
    errors: {},
    steps: ['Имя', 'Контакты', 'Карта', 'Пароль'],
    lastVisitedPage: REGISTER_BUTTON_PAGE_NUM,
    name: '',
    lastname: '',
    phone: '',
    email: '',
    card: '',
    password: '',
    password2: '',
  };

  fieldValidators = {
    name: (value) => REG_NAME.test(value),
    lastname: (value) => REG_NAME.test(value),
    phone: (value) => REG_PHONE.test(value),
    email: (value) => REG_EMAIL.test(value),
    card: (value) => value.length === 19 && !REG_CARD.test(value),
    password: (value) => value.length > 7,
    password2: (value) => value === this.state.password,
  };

  getInputColor = (e, inputState) => {
    if (inputState) {
      e.target.style.borderColor = '#46AD40';
    } else {
      e.target.style.borderColor = 'red';
    }
  };

  handleInputChange = (event) => {
    const { value, name } = event.target;
    const { fieldValidators, getInputColor } = this;

    this.setState((prevState) => ({
      [name]: value,
      errors: {
        ...prevState.errors,
        [name]: fieldValidators[name](value),
      },
    }));
    getInputColor(event, fieldValidators[name](value));
  };

  changeStateForNextPage = () => {
    let { pageNumber, lastVisitedPage } = this.state;

    if (pageNumber === lastVisitedPage) {
      lastVisitedPage++;
    }

    pageNumber++;
    this.setState({ pageNumber, lastVisitedPage });
  };

  nextPage = () => {
    const {
      changeStateForNextPage,
      state: {
        pageNumber,
        errors: { name, lastname, phone, email, card, password, password2 },
      },
    } = this;

    let triggerOnPageErrors = false;
    switch (pageNumber) {
      case REGISTER_BUTTON_PAGE_NUM:
        triggerOnPageErrors = true;
        break;

      case USER_DATA_PAGE_NUM:
        triggerOnPageErrors = name && lastname;
        break;

      case CONTACTS_PAGE_NUM:
        triggerOnPageErrors = phone && email;
        break;

      case CARD_PAGE_NUM:
        triggerOnPageErrors = card;
        break;

      case PASSWORD_PAGE_NUM:
        triggerOnPageErrors = password && password2;
        break;

      default:
        break;
    }
    triggerOnPageErrors && changeStateForNextPage();
  };

  prevPage = () => {
    let { pageNumber } = this.state;

    if (pageNumber > 1) {
      this.setState({ pageNumber: pageNumber - 1 });
    }
  };

  jumpToPage = (pageOnClick) => {
    const { lastVisitedPage, errors } = this.state;
    let triggerOnAnyError;

    for (const key in errors) {
      if (errors[key] === false) {
        triggerOnAnyError = true;
      }
    }

    if (pageOnClick <= lastVisitedPage && !triggerOnAnyError) {
      this.setState({ pageNumber: pageOnClick });
    }
  };

  renderWizardNavigation = () => {
    const {
      jumpToPage,
      state: { steps, pageNumber, lastVisitedPage },
    } = this;

    if (pageNumber < USER_DATA_PAGE_NUM || pageNumber > PASSWORD_PAGE_NUM) {
      return null;
    }

    return (
      <div className="navigation">
        <WizardNavigation
          steps={steps}
          currentPage={pageNumber}
          jumpToPage={jumpToPage}
          lastVisitedPage={lastVisitedPage}
        />
      </div>
    );
  };

  handleEnter = (event) => {
    if (event.key === 'Enter') {
      this.nextPage();
    }
  };

  renderWizardPage = () => {
    const {
      nextPage,
      handleInputChange,
      state: {
        pageNumber,
        name,
        lastname,
        phone,
        email,
        card,
        password,
        password2,
      },
    } = this;

    switch (pageNumber) {
      case REGISTER_BUTTON_PAGE_NUM:
        return (
          <button className="btn" onClick={nextPage} autoFocus>
            Зарегистрироваться
          </button>
        );

      case USER_DATA_PAGE_NUM:
        return (
          <UserData
            handler={handleInputChange}
            valueInputName={name}
            valueInputLastname={lastname}
          />
        );

      case CONTACTS_PAGE_NUM:
        return (
          <Contacts
            handler={handleInputChange}
            valueInputPhone={phone}
            valueInputEmail={email}
          />
        );

      case CARD_PAGE_NUM:
        return <Card handler={handleInputChange} valueInputCard={card} />;
      case PASSWORD_PAGE_NUM:
        return (
          <Password
            handler={handleInputChange}
            valueInputPass={password}
            valueInputPass2={password2}
          />
        );
      case CONGRATULATIONS_PAGE_NUM:
        return (
          <div className="congratulations">
            <div>
              Уважаемый, {name} {lastname}, вы успешно зарегистрированы.
            </div>
            <div>Email: {email}</div>
            <div>Номер телефона: {phone}</div>
          </div>
        );

      default:
        return null;
    }
  };

  render() {
    const {
      renderWizardNavigation,
      renderWizardPage,
      handleEnter,
      nextPage,
      prevPage,
      state: { pageNumber },
    } = this;

    return (
      <>
        {renderWizardNavigation()}
        <div className="wrapper" onKeyPress={handleEnter}>
          {renderWizardPage()}
          <Buttons
            prevPage={prevPage}
            nextPage={nextPage}
            pageNumber={pageNumber}
          />
        </div>
      </>
    );
  }
}

export default Wrapper;
