const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {
  entry: 
    './src/index.js',
    
  output: {
      filename: 'main.js',
      path: path.resolve(__dirname, '')
    },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader"
      },
      
      {
        test: /\.css$/,
        use: [
            "style-loader",
            "css-loader"
        ]
      },

      {
        test: /\.html$/i,
        loader: 'html-loader',
      },
      
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/template.html',
    }),
  ],
};
 
