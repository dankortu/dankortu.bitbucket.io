import './style.css';
import Sortable from "sortablejs";

var table = document.getElementById('myTable');
var tbody = table.getElementsByTagName("TBODY")[0];
var td = [];
var row = [];
var j = localStorage.getItem('rowCounter');


for (let u = 0; u < localStorage.getItem("rowCounter"); u++) {

  row[u] = document.createElement("TR")

  for (let i = 0 ; i < 3; i++) {
    let tdNode = document.createElement("td")
    tdNode.contentEditable = true;

    if (localStorage['localTd' + (u + 1) + '_' + (i + 1)] === undefined) {
      tdNode.innerHTML = '';
    } else if (i < 2) {
      tdNode.innerHTML = localStorage.getItem('localTd' + (u + 1) + '_' + (i + 1)); 
    } else {
    tdNode.contentEditable = false;
    tdNode.className = "handle";
    let colorPicker = tdNode.appendChild( document.createElement('input') );
    colorPicker.type ="color";
    colorPicker.value = localStorage.getItem('localTd' + (u + 1) + '_' + (i + 1));
    }

    row[u].appendChild(tdNode);
  } 

  tbody.appendChild( row[u] );
}


window.addRow = function addRow() {
  j++;
  row[j] = document.createElement("TR")

  for (let i = 1; i < 4; i++) {
    let tdNode = document.createElement("td")
    tdNode.contentEditable = true;

    if (i == 3) {
      tdNode.contentEditable = false;
      let colorPicker = tdNode.appendChild( document.createElement('input') );
      colorPicker.type ="color";
      tdNode.className = "handle";
    }

    row[j].appendChild(tdNode);
    console.log(j)
  } 

  tbody.appendChild( row[j] );

}

Sortable.create( tbody,
  {
    animation: 150,
    scroll: true,
    handle: '.handle',
  }
);


window.delRow = function delRow() {
  var lastRow = tbody.lastElementChild;

  if (j >= 1) {
    tbody.removeChild(lastRow);

    for (let i = 1; i < 4; i++) {
      localStorage.removeItem('localTd' + j + '_' + i);
    }
    j--;
  } else {
    alert('Записей нет');
  }
};

window.addEventListener('beforeunload', function() {
  for (let u = 0; u < j; u++) {
    row[u] = tbody.getElementsByTagName("tr")[u]
    for (let i = 0; i < 3; i++) {
      
      if (i < 2) {
        td[Number(String(u+1)+(i+1))] = row[u].getElementsByTagName("td")[i].innerHTML;
        localStorage['localTd' + (u+1) + "_" + (i+1)] = td[Number(String(u+1)+(i+1))];
      } else {
        td[Number(String(u+1)+(i+1))] = row[u].getElementsByTagName("td")[i].getElementsByTagName("input")[0].value;
        localStorage['localTd' + (u+1) + "_" + (i+1)] = td[Number(String(u+1)+(i+1))];
    
      }
    }
    
  }
  localStorage.setItem("rowCounter", j); 
});

