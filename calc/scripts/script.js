$( document ).ready(function() {
  $('.btn-green').click(function(){
      $('main').css('filter','blur(5px)');
      $('.js-overlay-campaign').fadeIn();
  });
  
  $('.btn-send').click(function(){
    $.post(
      "send.php",
      {
        u_name: $(".user-name").val(),
        u_email: $(".user-email").val()
      },
      onAjaxSuccess
    );
     
    function onAjaxSuccess(data)
    {

      alert(data);
    }
  });

  $(document).mouseup(function(e){
  var popup =$('.js-popup-campaign');
  if (e.target!=popup[0]&&popup.has(e.target).length===0){
      $('.js-overlay-campaign').fadeOut();
      $('main').css('filter','none');
  }
  })

  var slider = document.getElementById("myRange");
  var output = document.getElementById("demo");
  var slider1 = document.getElementById("myRange1");
  var output1 = document.getElementById("demo1");
  output.innerHTML = slider.value;
  slider.oninput = function() {
    output.innerHTML = this.value;
  }    
  output1.innerHTML = slider1.value;
  slider1.oninput = function() {
    output1.innerHTML = this.value;
  }

  $('.dropdown').click(function () {
    $(this).attr('tabindex', 1).focus();
    $(this).toggleClass('active');
    $(this).find('.dropdown__menu').slideToggle(300);
  });
  $('.dropdown').focusout(function () {
    $(this).removeClass('active');
    $(this).find('.dropdown__menu').slideUp(300);
  });
  $('.dropdown .dropdown__menu li').click(function () {
    $(this).parents('.dropdown').find('span').text($(this).text());
    $(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
  });

  $('.tab__question').click(function () {
    $(this).toggleClass('active');
  });
  $('.tab__question').focusout(function () {
    $(this).removeClass('active');
  });
});



